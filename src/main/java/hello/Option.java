package hello;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Option {
	private String id;
	private String name;
	private String countChosen;
	private String countSucceeded;
	
	public Option() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountChosen() {
		return countChosen;
	}

	public void setCountChosen(String countChosen) {
		this.countChosen = countChosen;
	}

	public String getCountSucceeded() {
		return countSucceeded;
	}

	public void setCountSucceeded(String countSucceeded) {
		this.countSucceeded = countSucceeded;
	}
	
	@Override
	public String toString() {
		return "Option{" +
				"id=" + id + 
				", name=" + name +
				", countChosen=" + countChosen +
				", countSucceeded=" + countSucceeded +
				"}";
	}
}
