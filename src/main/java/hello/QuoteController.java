package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

@Controller
public class QuoteController {
	@GetMapping("/quote")
	public String greeting(Model model) {
		// example rest call
		RestTemplate restTemplate = new RestTemplate();
		Quote quote = restTemplate.getForObject("http://gturnquist-quoters.cfapps.io/api/random", Quote.class);
		System.out.println(quote);
		String server = "localhost:8081"; // base url
		String app = "BanditExampleApp"; // name of the app
		String decision = "quote"; // name of the page

		String url = server + "/" + app + "/" + decision + "/" + "decide"; // url for getting a decision from Bandit

		// call the bandit and get the chosen option
		//Option banditsChosenOption = restTemplate.getForObject(url, Option.class);

		String view = "redquote";

		//if (banditsChosenOption != null) {
		//	view = banditsChosenOption.getName();
		//}
		return view;
	}
}
