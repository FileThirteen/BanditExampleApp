package hello;

import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ResultController {
	String[] quotes = {"Life is similar to a box of chocolates. -Marshall Ehlinger",
			"Home is the place is where your heart is located. -Marshall Ehlinger",
			"Vrroooooom. -Some Car", 
			"Tchk, Tchk, Tchk, Tchk. -Another Car", 
			"Autobots roll out! -Optimus Prime", 
			"I feel like a gold bug. -Bumblebee",
			"Gotta go fast -Sonic the Hedgehog"};
	
	@GetMapping("/result")
	public String greeting(Model model){
		Random rand = new Random();
		int randomValue = rand.nextInt(quotes.length);
		model.addAttribute("quote", quotes[randomValue]);
		return "result";
	}
}